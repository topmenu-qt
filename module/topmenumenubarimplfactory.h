/*
 * Copyright 2014 Javier S. Pedro <maemo@javispedro.com>
 *
 * This file is part of TopMenu.
 *
 * TopMenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TopMenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TopMenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOPMENUMENUBARIMPLFACTORY_H
#define TOPMENUMENUBARIMPLFACTORY_H

#include <QtCore/QObject>
#include <QtGui/private/qabstractplatformmenubar_p.h>

class Q_GUI_EXPORT TopMenuMenuBarImplFactory : public QObject, public QPlatformMenuBarFactoryInterface
{
	Q_OBJECT
	Q_INTERFACES(QPlatformMenuBarFactoryInterface:QFactoryInterface)
public:
	QAbstractPlatformMenuBar* create();
	QStringList keys() const;
};

#endif // TOPMENUMENUBARIMPLFACTORY_H
