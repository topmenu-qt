/*
 * Copyright 2014 Javier S. Pedro <maemo@javispedro.com>
 *
 * This file is part of TopMenu.
 *
 * TopMenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TopMenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TopMenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOPMENUMENUBARIMPL_H
#define TOPMENUMENUBARIMPL_H

#include <QtCore/QCoreApplication>
#include <QtCore/QDateTime>
#include <QtCore/QHash>
#include <QtCore/QTimer>
#include <QtGui/QAction>
#include <QtGui/QShortcut>
#include <QtGui/QX11Info>
#include <QtGui/private/qabstractplatformmenubar_p.h>
#include <QtGui/private/qt_x11_p.h>

#include "menuproxy.h"

class AppMenu;
typedef struct _TopMenuMonitor        TopMenuMonitor;
typedef struct _TopMenuAppMenuBar     TopMenuAppMenuBar;

class TopMenuMenuBarImpl : public MenuProxy, public QAbstractPlatformMenuBar {
    Q_OBJECT

public:
	explicit TopMenuMenuBarImpl(QObject *parent = 0);
	~TopMenuMenuBarImpl();

	void init(QMenuBar *);

	void setVisible(bool visible);

	void actionEvent(QActionEvent*);

	void handleReparent(QWidget *oldParent, QWidget *newParent, QWidget *oldWindow, QWidget *newWindow);

	bool allowCornerWidgets() const;

	void popupAction(QAction*);

	void setNativeMenuBar(bool);
	bool isNativeMenuBar() const;

	bool shortcutsHandledByNativeMenuBar() const;
	bool menuBarEventFilter(QObject *, QEvent *event);

	void updateServerAvailability();

protected:
	GtkMenuItem * addAction(QAction* action, QAction* before, QMenu* parent);
	void removeAction(QAction* action);
	void updateAction(QAction* action);

private:
	QShortcut * createMnemonicShortcut(QAction *action, QWidget *parent);
	static void handleMonitorAvailabilityChanged(TopMenuMenuBarImpl *self);

private slots:
	void handleShortcutActivated();

private:
	static bool staticInitialized;
	static TopMenuMonitor *menuMonitor;

	QWidget *m_window;
	QMenuBar *m_menubar;
	TopMenuAppMenuBar *m_appmenubar;
	AppMenu *m_appmenu;

	ulong m_monitor_connection_id;

	QList<QAction*> m_rootitems;

	bool m_disable;
};

#endif // TOPMENUMENUBARIMPL_H
