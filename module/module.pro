TEMPLATE = lib
TARGET = topmenu-qt4-module
QT       += core gui
CONFIG += plugin debug link_pkgconfig

SOURCES += topmenumenubarimpl.cc \
    topmenumenubarimplfactory.cc \
    qtkeysyms.cc \
    appmenu.cc \
    menuproxy.cc
HEADERS += topmenumenubarimpl.h \
    topmenumenubarimplfactory.h \
    qtkeysyms.h \
    appmenu.h \
    menuproxy.h

LIBS += -ltopmenu-client-gtk2
PKGCONFIG += gtk+-2.0 gthread-2.0

target.path += $$[QT_INSTALL_PLUGINS]/menubar
INSTALLS += target
