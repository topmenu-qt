/*
 * Copyright 2014 Javier S. Pedro <maemo@javispedro.com>
 *
 * This file is part of TopMenu.
 *
 * TopMenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TopMenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TopMenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPMENU_H
#define APPMENU_H

#include "menuproxy.h"

class AppMenu : public MenuProxy
{
	Q_OBJECT

public:
	AppMenu(QObject *parent = 0);
	~AppMenu();

	GtkMenu * getGtkMenu();

	GtkMenuItem * addAction(QAction *action);
	GtkMenuItem * addAction(QAction* action, QAction* before, QMenu* parent);
	void removeAction(QAction* action);
	void updateAction(QAction* action);

private:
	void addDefaultItems();
	static uint orderForRole(QAction::MenuRole role);
	static QAction::MenuRole detectRole(QAction *action);

private:
	GtkMenu *m_menu;
	QMultiMap<uint, QAction*> m_actions;
	QAction *m_def_quit;
};

#endif // APPMENU_H
