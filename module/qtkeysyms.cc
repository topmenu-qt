/****************************************************************************
**
** Copyright (C) 1992-2006 TROLLTECH ASA. All rights reserved.
**
** This file is part of the Phone Edition of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include <gdk/gdkkeysyms.h>
#include <QtCore/qcoreevent.h>

unsigned int qt_to_gdk_key(unsigned int qt_key) {
	unsigned int keysym = 0;
	switch (qt_key) {
	case Qt::Key_Escape: keysym = GDK_KEY_Escape; break;
	case Qt::Key_Tab: keysym = GDK_KEY_Tab; break;
	case Qt::Key_Backtab: keysym = GDK_KEY_ISO_Left_Tab; break;
	case Qt::Key_Backspace: keysym = GDK_KEY_BackSpace; break;
	case Qt::Key_Return: keysym = GDK_KEY_Return; break;
	case Qt::Key_Enter: keysym = GDK_KEY_KP_Enter; break;
	case Qt::Key_Insert: keysym = GDK_KEY_KP_Insert; break;
	case Qt::Key_Delete: keysym = GDK_KEY_KP_Delete; break;
	case Qt::Key_Pause: keysym = GDK_KEY_Pause; break;
	case Qt::Key_Print: keysym = GDK_KEY_Print; break;
	case Qt::Key_SysReq: keysym = 0x1005FF60; break;
	case Qt::Key_Clear: keysym = GDK_KEY_KP_Begin; break;
	case Qt::Key_Home: keysym = GDK_KEY_Home; break;
	case Qt::Key_End: keysym = GDK_KEY_End; break;
	case Qt::Key_Left: keysym = GDK_KEY_Left; break;
	case Qt::Key_Up: keysym = GDK_KEY_Up; break;
	case Qt::Key_Right: keysym = GDK_KEY_Right; break;
	case Qt::Key_Down: keysym = GDK_KEY_Down; break;
	case Qt::Key_PageUp: keysym = GDK_KEY_Prior; break;
	case Qt::Key_PageDown: keysym = GDK_KEY_Next; break;
	case Qt::Key_Shift: keysym = GDK_KEY_Shift_L; break;
	case Qt::Key_Control: keysym = GDK_KEY_Control_L; break;
	case Qt::Key_Meta: keysym = GDK_KEY_Meta_L; break;
	case Qt::Key_Alt: keysym = GDK_KEY_Alt_L; break;
	case Qt::Key_CapsLock: keysym = GDK_KEY_Caps_Lock; break;
	case Qt::Key_NumLock: keysym = GDK_KEY_Num_Lock; break;
	case Qt::Key_ScrollLock: keysym = GDK_KEY_Scroll_Lock; break;
	case Qt::Key_F1: keysym = GDK_KEY_F1; break;
	case Qt::Key_F2: keysym = GDK_KEY_F2; break;
	case Qt::Key_F3: keysym = GDK_KEY_F3; break;
	case Qt::Key_F4: keysym = GDK_KEY_F4; break;
	case Qt::Key_F5: keysym = GDK_KEY_F5; break;
	case Qt::Key_F6: keysym = GDK_KEY_F6; break;
	case Qt::Key_F7: keysym = GDK_KEY_F7; break;
	case Qt::Key_F8: keysym = GDK_KEY_F8; break;
	case Qt::Key_F9: keysym = GDK_KEY_F9; break;
	case Qt::Key_F10: keysym = GDK_KEY_F10; break;
	case Qt::Key_F11: keysym = GDK_KEY_F11; break;
	case Qt::Key_F12: keysym = GDK_KEY_F12; break;
	case Qt::Key_F13: keysym = GDK_KEY_F13; break;
	case Qt::Key_F14: keysym = GDK_KEY_F14; break;
	case Qt::Key_F15: keysym = GDK_KEY_F15; break;
	case Qt::Key_F16: keysym = GDK_KEY_F16; break;
	case Qt::Key_F17: keysym = GDK_KEY_F17; break;
	case Qt::Key_F18: keysym = GDK_KEY_F18; break;
	case Qt::Key_F19: keysym = GDK_KEY_F19; break;
	case Qt::Key_F20: keysym = GDK_KEY_F20; break;
	case Qt::Key_F21: keysym = GDK_KEY_F21; break;
	case Qt::Key_F22: keysym = GDK_KEY_F22; break;
	case Qt::Key_F23: keysym = GDK_KEY_F23; break;
	case Qt::Key_F24: keysym = GDK_KEY_F24; break;
	case Qt::Key_F25: keysym = GDK_KEY_F25; break;
	case Qt::Key_F26: keysym = GDK_KEY_F26; break;
	case Qt::Key_F27: keysym = GDK_KEY_F27; break;
	case Qt::Key_F28: keysym = GDK_KEY_F28; break;
	case Qt::Key_F29: keysym = GDK_KEY_F29; break;
	case Qt::Key_F30: keysym = GDK_KEY_F30; break;
	case Qt::Key_F31: keysym = GDK_KEY_F31; break;
	case Qt::Key_F32: keysym = GDK_KEY_F32; break;
	case Qt::Key_F33: keysym = GDK_KEY_F33; break;
	case Qt::Key_F34: keysym = GDK_KEY_F34; break;
	case Qt::Key_F35: keysym = GDK_KEY_F35; break;
	case Qt::Key_Super_L: keysym = GDK_KEY_Super_L; break;
	case Qt::Key_Super_R: keysym = GDK_KEY_Super_R; break;
	case Qt::Key_Menu: keysym = GDK_KEY_Menu; break;
	case Qt::Key_Hyper_L: keysym = GDK_KEY_Hyper_L; break;
	case Qt::Key_Hyper_R: keysym = GDK_KEY_Hyper_R; break;
	case Qt::Key_Help: keysym = GDK_KEY_Help; break;

	case Qt::Key_Space: keysym = GDK_KEY_space; break;
	case Qt::Key_Exclam: keysym = GDK_KEY_exclam; break;
	case Qt::Key_QuoteDbl: keysym = GDK_KEY_quotedbl; break;
	case Qt::Key_NumberSign: keysym = GDK_KEY_numbersign; break;
	case Qt::Key_Dollar: keysym = GDK_KEY_dollar; break;
	case Qt::Key_Percent: keysym = GDK_KEY_percent; break;
	case Qt::Key_Ampersand: keysym = GDK_KEY_ampersand; break;
	case Qt::Key_Apostrophe: keysym = GDK_KEY_apostrophe; break;
	case Qt::Key_ParenLeft: keysym = GDK_KEY_parenleft; break;
	case Qt::Key_ParenRight: keysym = GDK_KEY_parenright; break;
	case Qt::Key_Asterisk: keysym = GDK_KEY_asterisk; break;
	case Qt::Key_Plus: keysym = GDK_KEY_plus; break;
	case Qt::Key_Comma: keysym = GDK_KEY_comma; break;
	case Qt::Key_Minus: keysym = GDK_KEY_minus; break;
	case Qt::Key_Period: keysym = GDK_KEY_period; break;
	case Qt::Key_Slash: keysym = GDK_KEY_slash; break;
	case Qt::Key_0: keysym = GDK_KEY_0; break;
	case Qt::Key_1: keysym = GDK_KEY_1; break;
	case Qt::Key_2: keysym = GDK_KEY_2; break;
	case Qt::Key_3: keysym = GDK_KEY_3; break;
	case Qt::Key_4: keysym = GDK_KEY_4; break;
	case Qt::Key_5: keysym = GDK_KEY_5; break;
	case Qt::Key_6: keysym = GDK_KEY_6; break;
	case Qt::Key_7: keysym = GDK_KEY_7; break;
	case Qt::Key_8: keysym = GDK_KEY_8; break;
	case Qt::Key_9: keysym = GDK_KEY_9; break;
	case Qt::Key_Colon: keysym = GDK_KEY_colon; break;
	case Qt::Key_Semicolon: keysym = GDK_KEY_semicolon; break;
	case Qt::Key_Less: keysym = GDK_KEY_less; break;
	case Qt::Key_Equal: keysym = GDK_KEY_equal; break;
	case Qt::Key_Greater: keysym = GDK_KEY_greater; break;
	case Qt::Key_Question: keysym = GDK_KEY_question; break;
	case Qt::Key_At: keysym = GDK_KEY_at; break;
	case Qt::Key_A: keysym = GDK_KEY_a; break; // Must be lower case keysyms
	case Qt::Key_B: keysym = GDK_KEY_b; break; // for correct shift handling.
	case Qt::Key_C: keysym = GDK_KEY_c; break;
	case Qt::Key_D: keysym = GDK_KEY_d; break;
	case Qt::Key_E: keysym = GDK_KEY_e; break;
	case Qt::Key_F: keysym = GDK_KEY_f; break;
	case Qt::Key_G: keysym = GDK_KEY_g; break;
	case Qt::Key_H: keysym = GDK_KEY_h; break;
	case Qt::Key_I: keysym = GDK_KEY_i; break;
	case Qt::Key_J: keysym = GDK_KEY_j; break;
	case Qt::Key_K: keysym = GDK_KEY_k; break;
	case Qt::Key_L: keysym = GDK_KEY_l; break;
	case Qt::Key_M: keysym = GDK_KEY_m; break;
	case Qt::Key_N: keysym = GDK_KEY_n; break;
	case Qt::Key_O: keysym = GDK_KEY_o; break;
	case Qt::Key_P: keysym = GDK_KEY_p; break;
	case Qt::Key_Q: keysym = GDK_KEY_q; break;
	case Qt::Key_R: keysym = GDK_KEY_r; break;
	case Qt::Key_S: keysym = GDK_KEY_s; break;
	case Qt::Key_T: keysym = GDK_KEY_t; break;
	case Qt::Key_U: keysym = GDK_KEY_u; break;
	case Qt::Key_V: keysym = GDK_KEY_v; break;
	case Qt::Key_W: keysym = GDK_KEY_w; break;
	case Qt::Key_X: keysym = GDK_KEY_x; break;
	case Qt::Key_Y: keysym = GDK_KEY_y; break;
	case Qt::Key_Z: keysym = GDK_KEY_z; break;
	case Qt::Key_BracketLeft: keysym = GDK_KEY_bracketleft; break;
	case Qt::Key_Backslash: keysym = GDK_KEY_backslash; break;
	case Qt::Key_BracketRight: keysym = GDK_KEY_bracketright; break;
	case Qt::Key_AsciiCircum: keysym = GDK_KEY_asciicircum; break;
	case Qt::Key_Underscore: keysym = GDK_KEY_underscore; break;
	case Qt::Key_QuoteLeft: keysym = GDK_KEY_quoteleft; break;
	case Qt::Key_BraceLeft: keysym = GDK_KEY_braceleft; break;
	case Qt::Key_Bar: keysym = GDK_KEY_bar; break;
	case Qt::Key_BraceRight: keysym = GDK_KEY_braceright; break;
	case Qt::Key_AsciiTilde: keysym = GDK_KEY_asciitilde; break;

	case Qt::Key_nobreakspace: keysym = GDK_KEY_nobreakspace; break;
	case Qt::Key_exclamdown: keysym = GDK_KEY_exclamdown; break;
	case Qt::Key_cent: keysym = GDK_KEY_cent; break;
	case Qt::Key_sterling: keysym = GDK_KEY_sterling; break;
	case Qt::Key_currency: keysym = GDK_KEY_currency; break;
	case Qt::Key_yen: keysym = GDK_KEY_yen; break;
	case Qt::Key_brokenbar: keysym = GDK_KEY_brokenbar; break;
	case Qt::Key_section: keysym = GDK_KEY_section; break;
	case Qt::Key_diaeresis: keysym = GDK_KEY_diaeresis; break;
	case Qt::Key_copyright: keysym = GDK_KEY_copyright; break;
	case Qt::Key_ordfeminine: keysym = GDK_KEY_ordfeminine; break;
	case Qt::Key_guillemotleft: keysym = GDK_KEY_guillemotleft; break;
	case Qt::Key_notsign: keysym = GDK_KEY_notsign; break;
	case Qt::Key_hyphen: keysym = GDK_KEY_hyphen; break;
	case Qt::Key_registered: keysym = GDK_KEY_registered; break;
	case Qt::Key_macron: keysym = GDK_KEY_macron; break;
	case Qt::Key_degree: keysym = GDK_KEY_degree; break;
	case Qt::Key_plusminus: keysym = GDK_KEY_plusminus; break;
	case Qt::Key_twosuperior: keysym = GDK_KEY_twosuperior; break;
	case Qt::Key_threesuperior: keysym = GDK_KEY_threesuperior; break;
	case Qt::Key_acute: keysym = GDK_KEY_acute; break;
	case Qt::Key_mu: keysym = GDK_KEY_mu; break;
	case Qt::Key_paragraph: keysym = GDK_KEY_paragraph; break;
	case Qt::Key_periodcentered: keysym = GDK_KEY_periodcentered; break;
	case Qt::Key_cedilla: keysym = GDK_KEY_cedilla; break;
	case Qt::Key_onesuperior: keysym = GDK_KEY_onesuperior; break;
	case Qt::Key_masculine: keysym = GDK_KEY_masculine; break;
	case Qt::Key_guillemotright: keysym = GDK_KEY_guillemotright; break;
	case Qt::Key_onequarter: keysym = GDK_KEY_onequarter; break;
	case Qt::Key_onehalf: keysym = GDK_KEY_onehalf; break;
	case Qt::Key_threequarters: keysym = GDK_KEY_threequarters; break;
	case Qt::Key_questiondown: keysym = GDK_KEY_questiondown; break;
	case Qt::Key_Agrave: keysym = GDK_KEY_agrave; break;	// Lower case keysyms
	case Qt::Key_Aacute: keysym = GDK_KEY_aacute; break; // for shift handling.
	case Qt::Key_Acircumflex: keysym = GDK_KEY_acircumflex; break;
	case Qt::Key_Atilde: keysym = GDK_KEY_atilde; break;
	case Qt::Key_Adiaeresis: keysym = GDK_KEY_adiaeresis; break;
	case Qt::Key_Aring: keysym = GDK_KEY_aring; break;
	case Qt::Key_AE: keysym = GDK_KEY_ae; break;
	case Qt::Key_Ccedilla: keysym = GDK_KEY_ccedilla; break;
	case Qt::Key_Egrave: keysym = GDK_KEY_egrave; break;
	case Qt::Key_Eacute: keysym = GDK_KEY_eacute; break;
	case Qt::Key_Ecircumflex: keysym = GDK_KEY_ecircumflex; break;
	case Qt::Key_Ediaeresis: keysym = GDK_KEY_ediaeresis; break;
	case Qt::Key_Igrave: keysym = GDK_KEY_igrave; break;
	case Qt::Key_Iacute: keysym = GDK_KEY_iacute; break;
	case Qt::Key_Icircumflex: keysym = GDK_KEY_icircumflex; break;
	case Qt::Key_Idiaeresis: keysym = GDK_KEY_idiaeresis; break;
	case Qt::Key_ETH: keysym = GDK_KEY_eth; break;
	case Qt::Key_Ntilde: keysym = GDK_KEY_ntilde; break;
	case Qt::Key_Ograve: keysym = GDK_KEY_ograve; break;
	case Qt::Key_Oacute: keysym = GDK_KEY_oacute; break;
	case Qt::Key_Ocircumflex: keysym = GDK_KEY_ocircumflex; break;
	case Qt::Key_Otilde: keysym = GDK_KEY_otilde; break;
	case Qt::Key_Odiaeresis: keysym = GDK_KEY_odiaeresis; break;
	case Qt::Key_multiply: keysym = GDK_KEY_multiply; break;
	case Qt::Key_Ooblique: keysym = GDK_KEY_ooblique; break;
	case Qt::Key_Ugrave: keysym = GDK_KEY_ugrave; break;
	case Qt::Key_Uacute: keysym = GDK_KEY_uacute; break;
	case Qt::Key_Ucircumflex: keysym = GDK_KEY_ucircumflex; break;
	case Qt::Key_Udiaeresis: keysym = GDK_KEY_udiaeresis; break;
	case Qt::Key_Yacute: keysym = GDK_KEY_yacute; break;
	case Qt::Key_THORN: keysym = GDK_KEY_thorn; break;
	case Qt::Key_ssharp: keysym = GDK_KEY_ssharp; break;
	case Qt::Key_division: keysym = GDK_KEY_division; break;
	case Qt::Key_ydiaeresis: keysym = GDK_KEY_ydiaeresis; break;

	case Qt::Key_AltGr: keysym = GDK_KEY_ISO_Level3_Shift; break;
	case Qt::Key_Multi_key: keysym = GDK_KEY_Multi_key; break;
	case Qt::Key_Codeinput: keysym = GDK_KEY_Codeinput; break;
	case Qt::Key_SingleCandidate: keysym = GDK_KEY_SingleCandidate; break;
	case Qt::Key_MultipleCandidate: keysym = GDK_KEY_MultipleCandidate; break;
	case Qt::Key_PreviousCandidate: keysym = GDK_KEY_PreviousCandidate; break;

	case Qt::Key_Mode_switch: keysym = GDK_KEY_Mode_switch; break;

	case Qt::Key_Kanji: keysym = GDK_KEY_Kanji; break;
	case Qt::Key_Muhenkan: keysym = GDK_KEY_Muhenkan; break;
	case Qt::Key_Henkan: keysym = GDK_KEY_Henkan; break;
	case Qt::Key_Romaji: keysym = GDK_KEY_Romaji; break;
	case Qt::Key_Hiragana: keysym = GDK_KEY_Hiragana; break;
	case Qt::Key_Katakana: keysym = GDK_KEY_Katakana; break;
	case Qt::Key_Hiragana_Katakana: keysym = GDK_KEY_Hiragana_Katakana; break;
	case Qt::Key_Zenkaku: keysym = GDK_KEY_Zenkaku; break;
	case Qt::Key_Hankaku: keysym = GDK_KEY_Hankaku; break;
	case Qt::Key_Zenkaku_Hankaku: keysym = GDK_KEY_Zenkaku_Hankaku; break;
	case Qt::Key_Touroku: keysym = GDK_KEY_Touroku; break;
	case Qt::Key_Massyo: keysym = GDK_KEY_Massyo; break;
	case Qt::Key_Kana_Lock: keysym = GDK_KEY_Kana_Lock; break;
	case Qt::Key_Kana_Shift: keysym = GDK_KEY_Kana_Shift; break;
	case Qt::Key_Eisu_Shift: keysym = GDK_KEY_Eisu_Shift; break;
	case Qt::Key_Eisu_toggle: keysym = GDK_KEY_Eisu_toggle; break;

	case Qt::Key_Hangul: keysym = GDK_KEY_Hangul; break;
	case Qt::Key_Hangul_Start: keysym = GDK_KEY_Hangul_Start; break;
	case Qt::Key_Hangul_End: keysym = GDK_KEY_Hangul_End; break;
	case Qt::Key_Hangul_Hanja: keysym = GDK_KEY_Hangul_Hanja; break;
	case Qt::Key_Hangul_Jamo: keysym = GDK_KEY_Hangul_Jamo; break;
	case Qt::Key_Hangul_Romaja: keysym = GDK_KEY_Hangul_Romaja; break;
	case Qt::Key_Hangul_Jeonja: keysym = GDK_KEY_Hangul_Jeonja; break;
	case Qt::Key_Hangul_Banja: keysym = GDK_KEY_Hangul_Banja; break;
	case Qt::Key_Hangul_PreHanja: keysym = GDK_KEY_Hangul_PreHanja; break;
	case Qt::Key_Hangul_PostHanja: keysym = GDK_KEY_Hangul_PostHanja; break;
	case Qt::Key_Hangul_Special: keysym = GDK_KEY_Hangul_Special; break;

	case Qt::Key_Dead_Grave: keysym = GDK_KEY_dead_grave; break;
	case Qt::Key_Dead_Acute: keysym = GDK_KEY_dead_acute; break;
	case Qt::Key_Dead_Circumflex: keysym = GDK_KEY_dead_circumflex; break;
	case Qt::Key_Dead_Tilde: keysym = GDK_KEY_dead_tilde; break;
	case Qt::Key_Dead_Macron: keysym = GDK_KEY_dead_macron; break;
	case Qt::Key_Dead_Breve: keysym = GDK_KEY_dead_breve; break;
	case Qt::Key_Dead_Abovedot: keysym = GDK_KEY_dead_abovedot; break;
	case Qt::Key_Dead_Diaeresis: keysym = GDK_KEY_dead_diaeresis; break;
	case Qt::Key_Dead_Abovering: keysym = GDK_KEY_dead_abovering; break;
	case Qt::Key_Dead_Doubleacute: keysym = GDK_KEY_dead_doubleacute; break;
	case Qt::Key_Dead_Caron: keysym = GDK_KEY_dead_caron; break;
	case Qt::Key_Dead_Cedilla: keysym = GDK_KEY_dead_cedilla; break;
	case Qt::Key_Dead_Ogonek: keysym = GDK_KEY_dead_ogonek; break;
	case Qt::Key_Dead_Iota: keysym = GDK_KEY_dead_iota; break;
	case Qt::Key_Dead_Voiced_Sound: keysym = GDK_KEY_dead_voiced_sound; break;
	case Qt::Key_Dead_Semivoiced_Sound: keysym = GDK_KEY_dead_semivoiced_sound; break;
	case Qt::Key_Dead_Belowdot: keysym = GDK_KEY_dead_belowdot; break;
	case Qt::Key_Dead_Hook: keysym = GDK_KEY_dead_hook; break;
	case Qt::Key_Dead_Horn: keysym = GDK_KEY_dead_horn; break;
	}

	return keysym;
}
