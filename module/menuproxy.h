/*
 * Copyright 2014 Javier S. Pedro <maemo@javispedro.com>
 *
 * This file is part of TopMenu.
 *
 * TopMenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TopMenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TopMenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MENUPROXY_H
#define MENUPROXY_H

#include <QtGui/QAction>
#include <QtGui/QMenu>
#include <QtGui/QShortcut>

typedef struct _GtkMenu               GtkMenu;
typedef struct _GtkMenuShell          GtkMenuShell;
typedef struct _GtkMenuItem           GtkMenuItem;
typedef struct _GtkAccelGroup         GtkAccelGroup;

class MenuProxy : public QObject
{
	Q_OBJECT

public:
	explicit MenuProxy(QObject *parent = 0);
	~MenuProxy();

	void setTargetMenu(GtkMenuShell *shell);

public:
	virtual GtkMenuItem * addAction(QAction* action, QAction* before, QMenu* parent);
	virtual GtkMenu * addMenu(QMenu* menu);
	virtual void removeAction(QAction* action);
	virtual void removeMenu(QMenu* menu);
	virtual void updateAction(QAction* action);

	GtkMenuItem *getItemForAction(QAction *action);

	static QString transformMnemonic(const QString &text);

	bool eventFilter(QObject*, QEvent*);

private:
	static void handleMenuItemActivated(GtkMenuItem *item, QAction *action);
	static void handleMenuItemSelected(GtkMenuItem *item, QAction *action);
	static void handleMenuItemDeselected(GtkMenuItem *item, QAction *action);

private:
	GtkMenuShell *m_target;
	QHash<QMenu*, GtkMenu*> m_menus;
	QHash<QAction*, GtkMenuItem*> m_items;
	GtkAccelGroup *m_accel;
};

#endif // MENUPROXY_H
